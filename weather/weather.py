import os
import sys
import datetime
import requests
import pylunar
import configparser

config = configparser.ConfigParser()
module_dir = os.path.dirname(__file__)
config_path = os.path.join(module_dir, 'config.ini')
config.read(config_path)

geo_api_key = config.get('apikey', 'geo')
weather_api_key = config.get('apikey', 'weather')

def getLocationFromIp():
    url = f"https://ipinfo.io?token={geo_api_key}"
    json0 = requests.get(url).json()
    loc = json0["loc"].split(",")
    lat = float(loc[0])
    lon = float(loc[1])

    url = f"http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey={weather_api_key}&q={lat},{lon}"
    json1 = requests.get(url).json()

    return {
        'ip': json0["ip"],
        'key': json1["Key"],
        'latitude': lat,
        'longitude': lon,
        'city': json1["LocalizedName"],
        'country': json1["Country"]["LocalizedName"],
        'region': json1["Region"]["LocalizedName"]
    }

def getLocationFromQuery(query):
    url = f"http://dataservice.accuweather.com/locations/v1/cities/search?apikey={weather_api_key}&q={query}"
    json = requests.get(url).json()[0]

    return {
        'ip': "",
        'key': json["Key"],
        'latitude': float(json["GeoPosition"]["Latitude"]),
        'longitude': float(json["GeoPosition"]["Longitude"]),
        'city': json["LocalizedName"],
        'country': json["Country"]["LocalizedName"],
        'region': json["Region"]["LocalizedName"]
    }

def getWeather(locationId):
    url = f"http://dataservice.accuweather.com/currentconditions/v1/{locationId}?apikey={weather_api_key}"
    json = requests.get(url).json()[0]

    return {
        'description': json["WeatherText"],
        'temperature': json["Temperature"]["Metric"]["Value"],
        'unit': json["Temperature"]["Metric"]["Unit"],
        'icon_id': json["WeatherIcon"]
    }

def weatherEmoji(id):
    if id == 1 or id == 2: # Sunny
        return "☀️ "
    elif id == 3 or id == 21:
        return "🌤 "
    elif id == 4 or id == 5:
        return "⛅"
    elif id == 6 or id == 20:
        return "🌥 "
    elif id == 7 or id == 8 or id == 11 or id == 19 or id == 37 or id == 38:
        return "☁️ "
    elif id == 12 or id == 18 or id == 29 or id == 39 or id == 40:
        return "🌧️ "
    elif id == 13 or id == 14 or id == 16 or id == 17:
        return "🌦 "
    elif id == 15 or id == 41 or id == 42:
        return "🌩 "
    elif id == 22 or id == 23 or id == 25 or id == 26 or id == 43 or id == 44:
        return "🌨 "
    elif id == 24:
        return "🧊 "
    elif id == 30:
        return "🔥"
    elif id == 31:
        return "❄️ "
    elif id == 32:
        return "💨"
    elif id == 33 or id == 34 or id == 35 or id == 36:
        return "🌙"
    else:
        return f"? ({id})"

def locationEmoji(region):
    if region == "North America" or region == "South America":
        return "🌎"
    elif region == "Europe" or region == "Africa":
        return "🌍"
    elif region == "Asia" or region == "Australia":
        return "🌏"
    else:
        return f"? ({region})"

def coordToTuple(coord):
    l0 = int(coord)
    l1 = int(abs(coord * 100) % 100)
    l2 = int(abs(coord * 10000) % 100)
    return (l0, l1, l2)

def moonPhaseEmoji(lat, lon):
    lat = coordToTuple(lat)
    lon = coordToTuple(lon)
    moon = pylunar.MoonInfo(lat, lon)
    phase = moon.phase_name()

    if phase == "NEW_MOON":
        return "🌑"
    elif phase == "WAXING_CRESCENT":
        return "🌒"
    elif phase == "FIRST_QUARTER":
        return "🌓"
    elif phase == "WAXING_GIBBOUS":
        return "🌔"
    elif phase == "FULL_MOON":
        return "🌕"
    elif phase == "WANING_GIBBOUS":
        return "🌖"
    elif phase == "LAST_QUARTER":
        return "🌗"
    elif phase == "WANING_CRESCENT":
        return "🌘"
    else:
        return f"? ({phase})"

if len(sys.argv) > 1:
    try:
        location = getLocationFromQuery(sys.argv[1])
        weather = getWeather(location["key"])
        icon = weatherEmoji(weather["icon_id"])
        phase = moonPhaseEmoji(location["latitude"], location["longitude"])
        globe = locationEmoji(location["region"])

        print(f"{globe} {location['city']}, {location['country']} {icon} {weather['description']}, {weather['temperature']}°{weather['unit']} {phase}")

    except:
        print("Could not complete request.")

    finally:
        exit()

try:
    timestamp_cache = int(float(config.get('cache', 'timestamp')))
    timestamp = int(datetime.datetime.now().timestamp())
    if timestamp - timestamp_cache < 600:
        raise "Use cached values."

    location = getLocationFromIp()
    weather = getWeather(location["key"])
    icon = weatherEmoji(weather["icon_id"])
    phase = moonPhaseEmoji(location["latitude"], location["longitude"])
    globe = locationEmoji(location["region"])
    date = datetime.datetime.fromtimestamp(timestamp)

    print(f"{globe} {location['city']}, {location['country']} ({location['ip']}) {icon} {weather['description']}, {weather['temperature']}°{weather['unit']} {phase} ({date})")

    config.set('cache', 'ip', location['ip'])
    config.set('cache', 'city', location['city'])
    config.set('cache', 'country', location['country'])
    config.set('cache', 'region', location['region'])
    config.set('cache', 'latitude', str(location["latitude"]))
    config.set('cache', 'longitude', str(location["longitude"]))
    config.set('cache', 'description', weather['description'])
    config.set('cache', 'temp', str(weather['temperature']))
    config.set('cache', 'unit', weather['unit'])
    config.set('cache', 'icon_id', str(weather["icon_id"]))
    config.set('cache', 'timestamp', str(timestamp))

except:
    ip = config.get('cache', 'ip')
    city = config.get('cache', 'city')
    country = config.get('cache', 'country')
    region = config.get('cache', 'region')
    lat = float(config.get('cache', 'latitude'))
    lon = float(config.get('cache', 'longitude'))
    description = config.get('cache', 'description')
    temp = config.get('cache', 'temp')
    unit = config.get('cache', 'unit')
    iconId = int(config.get('cache', 'icon_id'))
    timestamp = int(float(config.get('cache', 'timestamp')))

    icon = weatherEmoji(iconId)
    phase = moonPhaseEmoji(lat, lon)
    globe = locationEmoji(region)
    date = datetime.datetime.fromtimestamp(timestamp)

    print(f"{globe} {city}, {country} ({ip}) {icon} {description}, {temp}°{unit} {phase} ({date})")

finally:
    file = open(config_path, 'w')
    config.write(file)
    file.close()
