git filter-branch -f --commit-filter '
        if [[ "$GIT_COMMITTER_NAME" = "Sean Midford" ]] || [[ "$GIT_COMMITTER_NAME" = "midfords" ]] || [[ "$GIT_COMMITTER_NAME" = "midfords-mac" ]];
        then
                GIT_COMMITTER_NAME="Sean Midford";
                GIT_AUTHOR_NAME="Sean Midford";
                GIT_COMMITTER_EMAIL="commits@seanmidford.ca";
                GIT_AUTHOR_EMAIL="commits@seanmidford.ca";
                git commit-tree "$@";
        else
                git commit-tree "$@";
        fi' -- --all
