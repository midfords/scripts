#!/bin/bash
#
# Perform backups of Wordpress site and SQL database.

. /home/ubuntu/wordpress-backup.config

date=`date +%d-%m-%Y`
path=$dir$date/

#######################################
# Run a function safely by silencing output,
#   printing success or failure message and 
#   notifying status by email
# Globals:
#   None
# Arguments:
#   Function to run
#   Success message
#   Failure message
# Returns:
#   None
#######################################
function run {
  if [ "$#" -lt 3 ]; then
    exit 1
  fi

  msg_succ=$1; shift
  msg_fail=$1; shift

  $@ > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    echo $msg_succ
  else
    echo $msg_fail
    notify_email $msg_fail
    exit 1
  fi
}

#######################################
# Send a status email if the script failed.
# Globals:
#   notification_email
# Arguments:
#   Error message
# Returns:
#   None
#######################################
function notify_email {
  echo -e "The wordpress backup for seanmidford.ca failed with error message: \n$@" | mail -s "seanmidford.ca Wordpress Backup Failed." $notification_email
}

function create_backup_dir {
  mkdir $path
}

function clean_backup_dir {
  rm -rf "${path}mysql-backup.sql" && \
  rm -rf "${path}wordpress-backup.tar.gz" && \
  rmdir $path
}

function backup_mysql {
  mysqldump -u $database_user -h $database_host $database_name > "${path}mysql-backup.sql"
}

function backup_wordpress {
  tar -czvf "${path}wordpress-backup.tar.gz" /var/www/wordpress
}

function upload_to_s3 {
  /usr/local/bin/aws2 s3 cp $1 s3://$s3_bucket_name/backups/$date/
}

run "Created backup directory." \
  "Failed to create backup directory, process stopped." \
  create_backup_dir

run "Created dump of SQL database." \
  "Failed to create dump of SQL database, process stopped." \
  backup_mysql

run "Created wordpress site backup." \
  "Failed to create wordpress site backup, process stopped." \
  backup_wordpress

run "Uploaded SQL backup to S3 bucket." \
  "Failed to upload SQL backup to S3 bucket." \
  upload_to_s3 "${path}mysql-backup.sql"

run "Uploaded wordpress site backup to S3 bucket." \
  "Failed to upload wordpress site backup to S3 bucket." \
  upload_to_s3 "${path}wordpress-backup.tar.gz"

run "Cleaned up backup directory." \
  "Failed to clean up backup directory, process stopped." \
  clean_backup_dir

echo "Script exited successfully."
